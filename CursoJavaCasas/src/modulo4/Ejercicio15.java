package modulo4;

import java.util.Scanner;

public class Ejercicio15 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Ingresar categoria de auto A, B o C");
		char Tipo = scan.next().charAt(0);
		
		switch (Tipo)
		{
		case 'A':
			System.out.println("Este tipo de autos tienen 4 ruedas y un motor");
			break;
		case 'B':
			System.out.println("Este tipo de autos tienen 4 ruedas, un motor, cierre centralizado y aire");
			break;
		case 'C':
			System.out.println("Este tipo de autos tienen 4 ruedas, un motor, cierre centralizado, aire y airbag.");
			break;
		default:
			System.out.println("Categoria invalida");
			break;
		}
		scan=null;


	}

}
