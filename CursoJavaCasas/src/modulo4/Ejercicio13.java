package modulo4;

import java.util.Scanner;

public class Ejercicio13 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Ingresar el numero de mes");
		int Mes = scan.nextInt();
		
		switch (Mes)
		{
		case 2: 
			System.out.println("El mes tiene 28 dias");
			break;
		case 4: case 6: case 9: case 11:
			System.out.println("El mes tiene 30 dias");
			break;
		case 1: case 3: case 5: case 7: case 8: case 12:
			System.out.println("El mes tiene 31 dias");
			break;
		default:
			System.out.println("No es un numero de mes valido");
			break;
		}
		scan=null;


	}

}
